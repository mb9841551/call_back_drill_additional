const fs = require("fs"); //Imported fs module
const path = require("path"); //Imported path module

function getAllCards(listId, callback) {
  setTimeout(() => {
    let listPath = path.join(__dirname, "cards_1.json"); //joined the cards_1.json to current directory
    fs.readFile(listPath, "utf-8", (err, listData) => {
      if (err) {
        return callback(err);
      }
      let objListData = JSON.parse(listData); //parse the data into JSON object
      callback(null, objListData[listId]); //invoke the callback function
    });
  }, 2 * 1000);
}

module.exports = getAllCards; //export the getAllCard function
