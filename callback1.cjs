const fs = require("fs"); //Imported fs module
const path = require("path"); //Imported path module

function boardInfo(boardId, callback) {
  setTimeout(() => {
    let boardFilePath = path.join(__dirname, "boards_1.json"); //joined the boards_1.json to current directory
    fs.readFile(boardFilePath, "utf-8", (err, data) => {
      if (err) {
        return callback(err);
      }
      // console.log(data);
      let boardInfo = JSON.parse(data); //parse the data into JSON object
      // console.log(boardInfo);
      let particularBoard = boardInfo.find((board) => board.id === boardId); //to find the particular board having particular id
      callback(null, particularBoard); //ionvoke callback function
    });
  }, 2 * 1000);
}

module.exports = boardInfo; //Exported the boardInfo function
