const fs = require("fs"); //Imported fs module
const path = require("path"); //Imported path module

function getAllLists(boardId, callback) {
  setTimeout(() => {
    let filePath = path.join(__dirname, "lists_1.json"); //joined the lists_1.json to current directory
    fs.readFile(filePath, "utf-8", (err, data) => {
      if (err) {
        return callback(err); //if err , call callback function
      }
      let objData = JSON.parse(data); //parse the data into JSON object
      callback(null, objData[boardId]); //invoke the callback function
    });
  }, 2 * 1000);
}

module.exports = getAllLists; //Exported the getAllLists function
