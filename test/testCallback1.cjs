const boardInfo = require("../callback1.cjs"); //Imported the function from callback1.cjs file

let boardId = "mcu453ed";

boardInfo(boardId, (err, data) => {
  //call the function which has a callback
  if (err) {
    console.log(err);
  }
  if (data) {
    console.log(data); //console data , id data is found
  } else {
    console.log("No boards found");
  }
});
