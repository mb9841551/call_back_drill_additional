const getAllLists = require("../callback2.cjs"); //Imported the function from callback2.cjs file

let boardId = "mcu453ed";

getAllLists(boardId, (err, data) => {
  //call the function which has a callback
  if (err) {
    console.log(err);
  }
  if (data) {
    console.log(data); //console data , id data is found
  } else {
    console.log("No lists found");
  }
});
