const getAllCards = require("../callback3.cjs"); //Imported the function from callback1.cjs file

let listId = "qwsa221";

getAllCards(listId, (err, data) => {
  //call the function which has a callback
  if (err) {
    console.log(err);
  }
  if (data) {
    console.log(data); //console data , id data is found
  } else {
    console.log("No cards found");
  }
});
