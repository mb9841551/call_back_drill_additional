const boardInfo = require("./callback1.cjs");  //Imported function from callback1.cjs file
const getAllLists = require("./callback2.cjs");  //Imported function from callback2.cjs file
const getAllCards = require("./callback3.cjs");  //Imported function from callback3.cjs file

const fs = require("fs"); //Imported fs module
const path = require("path"); //Imported path module

function getInfoAndCardFromAllList(callback) {
  setTimeout(() => {
    let boardFile = path.join(__dirname, "boards_1.json");  //joined the boards_1.json to current directory to a variable
    fs.readFile(boardFile, "utf-8", (err, data) => {
      if (err) {
        return callback(err);
      }
      let boardData = JSON.parse(data);   //parse the data into JSON object
      let thanosBoards = boardData.find((board) => board.name === "Thanos");    //to find the particular board having particular name "Thanos"
      let thanosId = thanosBoards.id;  //get id for thanos board
      // to get board for Thanos
      boardInfo(thanosId, (err, data) => {
        if (err) {
          console.log(err);
        }
        if (data) {
          console.log("Board = ", data);  //get boards object having Thanos in it
        }
        let listFile = path.join(__dirname, "lists_1.json");   //joined the lists_1.json to current directory to a variable
        fs.readFile(listFile, "utf-8", (err, data) => {
            if (err) {
              return callback(err);
            }
            let listData = JSON.parse(data);   //parse the data into JSON object
            
            getAllLists(thanosId, (err, data) => {
              if (err) {
                console.log(err);
              }
              if (data) {
                console.log("List = ", data);   //get lists object having Thanos id in it
              }
            });
            let cardList = listData[thanosId];  //get cardList object wh8ich has all powers of Thanos
            cardList.forEach((listId) => {
              getAllCards(listId.id, (err, data) => {   //call getAllcards function for each listId.id
                if (err) {
                  console.log(err);
                } else if (data) {
                  console.log(data);     //get cards object having Mind and space in it
                } else {
                  console.log(
                    `Power name ${listId.name} with id no ${listId.id} was not present in cards file`   //if object is not present
                  );
                }
              });
            });
          });
      });
    });
  }, 2000);
}

module.exports = getInfoAndCardFromAllList;   //exported the function
