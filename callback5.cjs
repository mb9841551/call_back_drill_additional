const boardInfo = require("./callback1.cjs"); //Imported function from callback1.cjs file
const getAllLists = require("./callback2.cjs"); //Imported function from callback2.cjs file
const getAllCards = require("./callback3.cjs"); //Imported function from callback3.cjs file

const fs = require("fs"); //Imported fs module
const path = require("path"); //Imported path module

function getInfoAndCardFromMindAndSpace(callback) {
  setTimeout(() => {
    let boardFile = path.join(__dirname, "boards_1.json"); //joined the boards_1.json to current directory to a variable
    fs.readFile(boardFile, "utf-8", (err, data) => {
      if (err) {
        return callback(err);
      }
      let boardData = JSON.parse(data); //parse the data into JSON object
      let thanosBoards = boardData.find((board) => board.name === "Thanos"); //to find the particular board having particular name "Thanos"
      let thanosId = thanosBoards.id; //get id for thanos board
      // to get board for Thanos
      boardInfo(thanosId, (err, data) => {
        if (err) {
          console.log(err);
        }
        if (data) {
          console.log("Boards = ", data); //get boards object having Thanos in it
        }
        let listFile = path.join(__dirname, "lists_1.json"); //joined the lists_1.json to current directory to a variable
        fs.readFile(listFile, "utf-8", (err, data) => {
          if (err) {
            return callback(err);
          }
          let listData = JSON.parse(data); //parse the data into JSON object
          getAllLists(thanosId, (err, data) => {
            if (err) {
              console.log(err);
            }
            if (data) {
              console.log("Lists = ", data); //get lists object having Thanos id in it
            }
            let cardList = listData[thanosId].filter(
              (power) => power.name === "Mind" || power.name === "Space"
            ); //get cardList object wh8ich has a power "Mind" and "Space"
            cardList.forEach((listId) => {
              //use forEach to iterate over cardList Array
              getAllCards(listId.id, (err, data) => {
                //call getAllcards function for each listId.id
                if (err) {
                  console.log(err);
                }
                if (data) {
                  console.log(data); //get cards object having Mind and space in it
                }
              });
            });
          });
        });
      });
    });
  }, 2000);
}

module.exports = getInfoAndCardFromMindAndSpace; //Exported the function
